Groupe :  
- Guillaume Migeon

Dépôt git :  
https://bitbucket.org/Outpox/framework  

Fonctionnalités :  
- Tout ce qui est affecté par le reset css a été refait à ma convenance  
C'est visible dans typo.html  
- Un système de grille est disponible et visible dans grille.html  
- Tout est fait en sass  
- Un seul fichier main.css est généré  

Guide :  
- Pour voir comment utiliser la typo, il suffit d'ouvrir typo.html, tout est expliqué dedans  
Pour la grille :  
- Une classe "grid" qui va englober toute la grille  
- Une class "row" pour une ligne  
- Chaque ligne fait 8 unités  
- Une classe "span-1" vaut une unité  
- Va de "span-1" à "span-8"  
- Offset disponible pour décaller ("offset-1" à "offset-7")  
- Le fichier grille.html montre la grille